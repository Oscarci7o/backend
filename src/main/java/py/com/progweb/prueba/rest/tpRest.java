package py.com.progweb.prueba.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import py.com.progweb.prueba.ejb.BolsaDAO;
import py.com.progweb.prueba.ejb.CabeceraDAO;
import py.com.progweb.prueba.ejb.ClienteDAO;
import py.com.progweb.prueba.ejb.ConceptoDAO;
import py.com.progweb.prueba.ejb.ReglaPuntosDAO;
import py.com.progweb.prueba.model.Bolsa;
import py.com.progweb.prueba.model.BolsaAux;
import py.com.progweb.prueba.model.CabeceraAux;
import py.com.progweb.prueba.model.Cliente;
import py.com.progweb.prueba.model.ClienteAux;
import py.com.progweb.prueba.model.Concepto;
import py.com.progweb.prueba.model.UsoPuntosAux;



@Path("tp")
@Consumes("application/json")
@Produces("application/json")
public class tpRest {
	@Inject
	private ClienteDAO clienteDAO;
	@Inject
	private BolsaDAO bolsaDAO;
	@Inject
	private CabeceraDAO cabeceraDAO;
	@Inject
	private ConceptoDAO conceptoDAO;
	@Inject
	private ReglaPuntosDAO reglaPuntosDAO;
	
	@GET
	@Path("/cliente")
	public Response listar() {
		return Response.ok(clienteDAO.lista()).build();
	}
	
	@POST
	@Path("/cliente")
	public Response crearCliente(ClienteAux p) {
		this.clienteDAO.agregarAux(p);
		return Response.ok().build();
	}
	
	@POST
	@Path("/concepto")
	public Response crearCliente(Concepto p) {
		this.conceptoDAO.agregar(p);
		return Response.ok().build();
	}
	
	@POST
	@Path("/bolsa")
	public Response crearBolsa(BolsaAux p) {
		this.bolsaDAO.agregarbolsa(p);
		return Response.ok().build();
	}
	
	@POST
	@Path("/uso")
	public Response uso(CabeceraAux p) {
		this.cabeceraDAO.agregarCabecera(p);
		return Response.ok().build();
	}
	
	
	@GET
	@Path("/consulta")
	public Response calcularPuntos(Integer monto) {
		
		Integer resultado = monto/reglaPuntosDAO.lista().get(0).getGuaranies();
		return Response.ok(resultado).build();
	}
	
	@GET
	@Path("/consultaUsoPuntos")
	public Response usoPuntos(UsoPuntosAux usoPuntosAux) throws ParseException {
		Long resultado = new Long(0);
		if(usoPuntosAux.getIdConcepto()!=null) {
			resultado = cabeceraDAO.consultaPorConcepto(usoPuntosAux.getIdConcepto());
		}else if(usoPuntosAux.getFechaUso()!=null) {
			System.out.println("la fecha es: "+usoPuntosAux.getFechaUso());
			resultado = cabeceraDAO.consultaPorFecha(usoPuntosAux.getFechaUso());
		}else if(usoPuntosAux.getIdCliente()!=null) {
			resultado = cabeceraDAO.consultaPorCliente(usoPuntosAux.getIdCliente());
		}
		
		return Response.ok(resultado).build();
	}
	
	@GET
	@Path("/consultaBolsa")
	public Response listaBolsas(BolsaAux bolsaAux) {
		List<Bolsa> resultado = new ArrayList<Bolsa>();
		if(bolsaAux.getIdCliente()!=null) {
			resultado = bolsaDAO.consultaPorCliente(bolsaAux.getIdCliente());
		}else if(bolsaAux.getRango()[0]!=null && bolsaAux.getRango()[1]!=null) {
			resultado = bolsaDAO.consultaPorRango(bolsaAux.getRango());
		}
		
		return Response.ok(resultado).build();
	}
	
	
	@GET
	@Path("/consultaClientesPuntosVencer")
	public Response clientesVencimiento(Integer dias) {
		List<Bolsa> resultado = new ArrayList<Bolsa>();
		
		resultado = bolsaDAO.consultaPuntosVencer(dias);
		
		
		return Response.ok(resultado).build();
	}
	
	@GET
	@Path("/consultaClientes")
	public Response clientesVencimiento(@QueryParam("nombre") String nombre, 
			@QueryParam("apellido") String apellido, @QueryParam("cumple") String cumple) {
		List<Cliente> resultado = new ArrayList<Cliente>();
		
		if(nombre!=null) {
			resultado=clienteDAO.getClientesNombre(nombre);
		}else if(apellido!=null) {
			resultado=clienteDAO.getClientesApellido(apellido);
		}else if(cumple!=null) {
			resultado=clienteDAO.getClientesCumpleano(cumple);
		}
		
		return Response.ok(resultado).build();
	}
	
	
	@GET
	@Path("/correrProceso")
	public Response correrProceso() {
		
		bolsaDAO.actualizarVencimiento();
		
		return Response.ok().build();
	}
	
}
