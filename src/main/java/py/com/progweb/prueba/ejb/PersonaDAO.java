package py.com.progweb.prueba.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Agenda;
import py.com.progweb.prueba.model.Persona;

@Stateless
public class PersonaDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	@Inject
	AgendaDAO agendaDAO;
	
	public void agregar(Persona entidad) {
		this.em.persist(entidad);
		for (Agenda a: entidad.getListaAgenda()) {
			a.setPersona(entidad);
			System.out.println("dentro de agregar la fecha es: "+a.getFecha());
			agendaDAO.agregar(a);
		}
	}
	
	public List<Persona> lista(){
		Query q = this.em.createQuery("select p from Persona p");
		return (List<Persona>) q.getResultList();
	}
}
