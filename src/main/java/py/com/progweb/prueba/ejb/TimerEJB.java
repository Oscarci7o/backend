package py.com.progweb.prueba.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
@LocalBean
public class TimerEJB {
	@Inject
	BolsaDAO bolsaDAO;
	
	@Schedule(second="01", minute="*", hour="*")
	public void execute() {
		bolsaDAO.actualizarVencimiento();
	}
}
