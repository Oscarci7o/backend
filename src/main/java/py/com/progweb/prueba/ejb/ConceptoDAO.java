package py.com.progweb.prueba.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Concepto;

@Stateless
public class ConceptoDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	public void agregar(Concepto entidad) {
		this.em.persist(entidad);
	}
	
	public List<Concepto> lista(){
		Query q = this.em.createQuery("select p from Concepto p");
		return (List<Concepto>) q.getResultList();
	}
	
	public Concepto getConceptoId(Integer id) {
		Query q = this.em.createQuery("select p from Concepto p where p.idConcepto = :id").setParameter("id", id);
		List<Concepto> lista = (List<Concepto>) q.getResultList();
		if(!lista.isEmpty()) {
			return  lista.get(0);
		}else {
			return null;
		}
		
	}
	
}
