package py.com.progweb.prueba.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Agenda;
import py.com.progweb.prueba.model.Persona;

@Stateless
public class AgendaDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void agregar(Agenda entidad) {
		this.em.persist(entidad);
	}
	
	/*public List<Persona> lista(){
		Query q = this.em.createQuery("select p from Persona p");
		return (List<Persona>) q.getResultList();
	}*/
}
