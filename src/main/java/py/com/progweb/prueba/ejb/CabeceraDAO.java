package py.com.progweb.prueba.ejb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Agenda;
import py.com.progweb.prueba.model.Bolsa;
import py.com.progweb.prueba.model.BolsaAux;
import py.com.progweb.prueba.model.Cabecera;
import py.com.progweb.prueba.model.CabeceraAux;
import py.com.progweb.prueba.model.Concepto;
import py.com.progweb.prueba.model.Detalle;

@Stateless
public class CabeceraDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	@Inject
	private ConceptoDAO conceptoDAO;
	@Inject
	private BolsaDAO bolsaDAO;
	@Inject
	private DetalleDAO detalleDAO;
	
	@Inject
	private MailEJB mailEJB;
	
	public void agregar(Cabecera entidad) {
		this.em.persist(entidad);
		for (Detalle a: entidad.getListaDetalle()) {
			a.setCabecera(entidad);;
			detalleDAO.agregar(a);
		}
	}
	
	public List<Cabecera> lista(){
		Query q = this.em.createQuery("select p from Cabecera p");
		return (List<Cabecera>) q.getResultList();
	}
	
	public void agregarCabecera(CabeceraAux p) {
		Cabecera cabecera = new Cabecera();
		Concepto concepto;
		Integer puntos = 0;
		List<Detalle> listaDetalle = new ArrayList<Detalle>();
		
		//realizar calculos
		Long tiempo = System.currentTimeMillis();
		Date date = new Date(tiempo);
		
		//sacar de concepto lo que cuesta y guardar en puntajeUsado de cabecera
		concepto=conceptoDAO.getConceptoId(p.getIdConcepto());
		cabecera.setPuntajeUsado(concepto.getPuntosRequeridos());
		
		//si tiene los puntos necesarios
		puntos = bolsaDAO.calcularPuntos(p.getIdCliente());
		if(puntos >= concepto.getPuntosRequeridos()) {
			cabecera.setIdCliente(p.getIdCliente());
			cabecera.setConceptoUso(p.getIdConcepto());
			cabecera.setFecha(date);
			
			//elegir la bolsa mas vieja o mas viejas
			listaDetalle = bolsaDAO.usarBolsas(p.getIdCliente(), cabecera.getIdCabecera(), concepto.getPuntosRequeridos());
			cabecera.setListaDetalle(listaDetalle);
			//guardar cabecera y detalles
			this.agregar(cabecera);
			try {
				mailEJB.enviarMail(concepto.getPuntosRequeridos(), p.getIdCliente());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		

	}
	
	public Long consultaPorConcepto(Integer concepto){
		Query q = this.em.createQuery("select sum(p.puntajeUsado) from Cabecera p where p.conceptoUso=:concepto")
				.setParameter("concepto", concepto);
		return (Long) q.getResultList().get(0);
	}
	
	public Long consultaPorFecha(String fechaString) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Date fecha = dateFormat.parse(fechaString);
		System.out.println(fecha);
		Query q = this.em.createQuery("select sum(p.puntajeUsado) from Cabecera p where p.fecha=:fecha")
				.setParameter("fecha", fecha);
		return (Long) q.getResultList().get(0);
	}
	
	public Long consultaPorCliente(Integer cliente){
		Query q = this.em.createQuery("select sum(p.puntajeUsado) from Cabecera p where p.idCliente=:cliente")
				.setParameter("cliente", cliente);
		return (Long) q.getResultList().get(0);
	}
}
