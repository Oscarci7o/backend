package py.com.progweb.prueba.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

import py.com.progweb.prueba.model.Cliente;
import py.com.progweb.prueba.model.Mail;

@Stateless
@LocalBean
public class MailEJB {
	@Inject
	MailDAO mailDAO;
	
	@Inject
	ClienteDAO clienteDAO;
	
	public void enviarMail(int puntos, int idCliente) throws Exception {
		//leer mail
		Mail mail = mailDAO.lista().get(0);
		
		Cliente cliente = clienteDAO.getClienteId(idCliente).get(0);
		
		String mailCliente = cliente.getEmail();
		
		Email email = new SimpleEmail();
		email.setHostName("smtp.googlemail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator(mail.getDireccion(), mail.getPass()));
		email.setSSLOnConnect(true);
		email.setFrom(mail.getDireccion());
		email.setSubject("Uso de puntos");
		email.setMsg("Se han debitado "+ puntos+ " puntos de su cuenta");
		//agregar a la lista de mails que tienen ese grupo
		
		email.addTo(mailCliente);
		
		email.send();
	}

}
