package py.com.progweb.prueba.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.ReglaPuntos;

@Stateless
public class ReglaPuntosDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void agregar(ReglaPuntos entidad) {
		this.em.persist(entidad);
	}
	
	public List<ReglaPuntos> lista(){
		Query q = this.em.createQuery("select p from ReglaPuntos p");
		return (List<ReglaPuntos>) q.getResultList();
	}
}
