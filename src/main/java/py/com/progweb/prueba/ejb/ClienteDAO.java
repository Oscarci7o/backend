package py.com.progweb.prueba.ejb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Cliente;
import py.com.progweb.prueba.model.ClienteAux;

@Stateless
public class ClienteDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	public void agregar(Cliente entidad) {
		this.em.persist(entidad);
	}
	
	public void agregarAux(ClienteAux entidadAux) {
		try {
			Cliente entidad = new Cliente();
			entidad.setApellido(entidadAux.getApellido());
			entidad.setEmail(entidadAux.getEmail());
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		    Date fecha;
			fecha = dateFormat.parse(entidadAux.getFechaNacimiento());
			entidad.setFechaNacimiento(fecha);
			entidad.setNacionalidad(entidadAux.getNacionalidad());
			entidad.setNombre(entidadAux.getNombre());
			entidad.setNroDocumento(entidadAux.getNroDocumento());
			entidad.setTelefono(entidadAux.getTelefono());
			entidad.setTipoDocumento(entidadAux.getTipoDocumento());
			this.em.persist(entidad);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Cliente> lista(){
		Query q = this.em.createQuery("select p from Cliente p");
		return (List<Cliente>) q.getResultList();
	}
	
	public List<Cliente> getClienteId(int id){
		Query q = this.em.createQuery("select p from Cliente p where idCliente=:idCliente").setParameter("idCliente", id);
		return (List<Cliente>) q.getResultList();
	}
	
	public List<Cliente> getClientesNombre(String nombre){
		Query q = this.em.createQuery("select p from Cliente p where p.nombre like :nombre")
				.setParameter("nombre", nombre+"%");
		return (List<Cliente>) q.getResultList();
	}
	
	public List<Cliente> getClientesApellido(String apellido){
		Query q = this.em.createQuery("select p from Cliente p where p.apellido like :apellido")
				.setParameter("apellido", apellido+"%");
		return (List<Cliente>) q.getResultList();
	}
	
	public List<Cliente> getClientesCumpleano(String fechaString){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		     Date fecha = dateFormat.parse(fechaString);
			Query q = this.em.createQuery("select p from Cliente p where p.fechaNacimiento=:fecha")
					.setParameter("fecha", fecha);
			return (List<Cliente>) q.getResultList();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
