package py.com.progweb.prueba.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Bolsa;
import py.com.progweb.prueba.model.BolsaAux;
import py.com.progweb.prueba.model.Concepto;
import py.com.progweb.prueba.model.Detalle;
import py.com.progweb.prueba.model.Persona;

@Stateless
public class BolsaDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	@Inject
	private ReglaPuntosDAO reglaPuntosDAO;
	
	public void agregar(Bolsa entidad) {
		this.em.persist(entidad);
	}
	
	public List<Bolsa> lista(){
		Query q = this.em.createQuery("select p from Bolsa p");
		return (List<Bolsa>) q.getResultList();
	}
	
	public void agregarbolsa(BolsaAux p) {
		Bolsa bolsa = new Bolsa();
		//realizar calculos
		Long tiempo = System.currentTimeMillis();
		Long tiempoAgregado = Long.valueOf("5180000000");
		Date date = new Date(tiempo);
		Date dateAgregado = new Date(tiempo+tiempoAgregado);
		
		bolsa.setFechaAsignacion(date);
		bolsa.setFechaCaducidad(dateAgregado);
		Integer puntaje = p.getMontoOperacion()/reglaPuntosDAO.lista().get(0).getGuaranies();
		bolsa.setPuntajeAsignado(puntaje);
		bolsa.setPuntajeUtilizado(0);
		bolsa.setSaldoPuntos(puntaje);
		bolsa.setMontoOperacion(p.getMontoOperacion());
		bolsa.setIdCliente(p.getIdCliente());
		//guardar en agregar
		this.agregar(bolsa);
	}
	
	public Integer calcularPuntos(Integer id) {
		Query q = this.em.createQuery("select p from Bolsa p where p.idCliente = :id").setParameter("id", id);
		List<Bolsa> lista = (List<Bolsa>) q.getResultList();
		Integer suma=0;
		for (Bolsa elemento : lista) {
			suma = suma + elemento.getSaldoPuntos();
		}
			
		return suma;
	}
	
	public List<Detalle>  usarBolsas(Integer idCliente, Integer idCabecera,Integer puntos) {
		Query q = this.em.createQuery("select p from Bolsa p where p.idCliente = :id order by p.idBolsa asc").setParameter("id", idCliente);
		List<Bolsa> lista = (List<Bolsa>) q.getResultList();
		Integer puntosRestantes=puntos;
		
		List<Detalle> listaDetalle = new ArrayList<Detalle>();
		
		for (Bolsa elemento : lista) {
			Detalle detalle = new Detalle();
			if(elemento.getSaldoPuntos()>puntosRestantes) {
				detalle.setIdBolsa(elemento.getIdBolsa());
				detalle.setPuntajeUsado(puntosRestantes);
				listaDetalle.add(detalle);
				
				elemento.setPuntajeUtilizado(elemento.getPuntajeUtilizado()+puntosRestantes);
				puntosRestantes = 0;
				elemento.setSaldoPuntos(elemento.getPuntajeAsignado()-elemento.getPuntajeUtilizado());

				
				break;
			}else if(elemento.getSaldoPuntos()<puntosRestantes) {
				detalle.setIdBolsa(elemento.getIdBolsa());
				detalle.setPuntajeUsado(elemento.getSaldoPuntos());
				listaDetalle.add(detalle);
				
				elemento.setPuntajeUtilizado(elemento.getPuntajeAsignado());
				puntosRestantes = puntosRestantes - elemento.getSaldoPuntos();
				elemento.setSaldoPuntos(0);
			}else {
				detalle.setIdBolsa(elemento.getIdBolsa());
				detalle.setPuntajeUsado(elemento.getSaldoPuntos());
				listaDetalle.add(detalle);
				
				elemento.setPuntajeUtilizado(elemento.getPuntajeAsignado());
				elemento.setSaldoPuntos(0);
				puntosRestantes = 0;
				break;
			}
		}
		
		return listaDetalle;

	}
	
	public List<Bolsa> consultaPorCliente(Integer cliente){
		Query q = this.em.createQuery("select p from Bolsa p where p.idCliente=:cliente")
				.setParameter("cliente", cliente);
		return (List<Bolsa>) q.getResultList();
	}
	
	public List<Bolsa> consultaPorRango(Integer[] rango){
		Query q = this.em.createQuery("select p from Bolsa p where p.saldoPuntos<=:rango2 and p.saldoPuntos>=:rango1")
				.setParameter("rango1", rango[0]).setParameter("rango2", rango[1]);
		return (List<Bolsa>) q.getResultList();
	}
	
	public List<Bolsa> consultaPuntosVencer(Integer dias){
		Long diasMilisegundos = (long) (dias * 86400000);
		Long tiempo = System.currentTimeMillis();
		Date hoy = new Date(tiempo);
		Date ultimoDia = new Date(tiempo+diasMilisegundos);
		Query q = this.em.createQuery("select distinct p.idCliente from Bolsa p where p.fechaCaducidad>=:hoy and p.fechaCaducidad<=:ultimoDia")
				.setParameter("hoy", hoy).setParameter("ultimoDia", ultimoDia);
		return (List<Bolsa>) q.getResultList();
	}
	
	public void actualizarVencimiento(){
		Long tiempo = System.currentTimeMillis();
		Date hoy = new Date(tiempo);
		Query q = this.em.createQuery("update Bolsa set saldoPuntos = 0 WHERE saldoPuntos > 0 and fechaCaducidad < :hoy")
				.setParameter("hoy", hoy);
		q.executeUpdate();
	}
	
}
