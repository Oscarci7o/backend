package py.com.progweb.prueba.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.progweb.prueba.model.Detalle;

@Stateless
public class DetalleDAO {
	@PersistenceContext(unitName = "pruebaPU")
	private EntityManager em;
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void agregar(Detalle entidad) {
		this.em.persist(entidad);
	}
	
	public List<Detalle> lista(){
		Query q = this.em.createQuery("select p from Detalle p");
		return (List<Detalle>) q.getResultList();
	}
	
}
