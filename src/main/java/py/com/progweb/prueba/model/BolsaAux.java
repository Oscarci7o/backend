package py.com.progweb.prueba.model;


public class BolsaAux {
	
	private Integer idCliente;
	
	private Integer montoOperacion;
	
	private Integer saldoPuntos;
	
	private Integer[] rango;
	

	public BolsaAux() {

	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Integer getMontoOperacion() {
		return montoOperacion;
	}


	public void setMontoOperacion(Integer montoOperacion) {
		this.montoOperacion = montoOperacion;
	}


	public Integer getSaldoPuntos() {
		return saldoPuntos;
	}


	public void setSaldoPuntos(Integer saldoPuntos) {
		this.saldoPuntos = saldoPuntos;
	}


	public Integer[] getRango() {
		return rango;
	}


	public void setRango(Integer[] rango) {
		this.rango = rango;
	}

	
	
	
}
