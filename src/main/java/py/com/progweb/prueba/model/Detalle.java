package py.com.progweb.prueba.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "detalle")

public class Detalle {
	@Id
	@Column(name="id_detalle")
	@Basic(optional = false)
	@GeneratedValue(generator = "detalleSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "detalleSec", sequenceName = "detalle_sec", allocationSize = 0)
	private Integer idDetalle;
	
	@Column(name = "puntaje_usado")
	@Basic(optional = false)
	private Integer puntajeUsado;
	
	@Column(name = "id_bolsa")
	@Basic(optional = false)
	private Integer idBolsa;
	
	@JoinColumn(name = "id_cabecera", referencedColumnName = "id_cabecera")
	@ManyToOne(optional = false)
	private Cabecera cabecera;
	
	

	public Detalle() {

	}



	public Integer getIdDetalle() {
		return idDetalle;
	}



	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}



	public Integer getPuntajeUsado() {
		return puntajeUsado;
	}



	public void setPuntajeUsado(Integer puntajeUsado) {
		this.puntajeUsado = puntajeUsado;
	}



	public Integer getIdBolsa() {
		return idBolsa;
	}



	public void setIdBolsa(Integer idBolsa) {
		this.idBolsa = idBolsa;
	}


	@JsonBackReference
	public Cabecera getCabecera() {
		return cabecera;
	}



	public void setCabecera(Cabecera cabecera) {
		this.cabecera = cabecera;
	}
	
	
	
	
	
	
}
