package py.com.progweb.prueba.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "cliente")

public class Cliente {
	@Id
	@Column(name="id_cliente")
	@Basic(optional = false)
	@GeneratedValue(generator = "clienteSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "clienteSec", sequenceName = "cliente_sec", allocationSize = 0)
	private Integer idCliente;

	@Column(name = "nombre")
	@Basic(optional = false)
	private String nombre;
	
	@Column(name = "apellido")
	@Basic(optional = false)
	private String apellido;
	
	@Column(name = "nro_documento")
	@Basic(optional = false)
	private Integer nroDocumento;
	
	@Column(name = "tipo_documento")
	@Basic(optional = false)
	private String tipoDocumento;
	
	@Column(name = "nacionalidad")
	@Basic(optional = false)
	private String nacionalidad;
	
	@Column(name = "email")
	@Basic(optional = false)
	private String email;
	
	@Column(name = "telefono")
	@Basic(optional = false)
	private Integer telefono;

	@Column(name = "fecha_nacimiento")
	@Basic(optional = false)
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	

	public Cliente() {

	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public Integer getNroDocumento() {
		return nroDocumento;
	}


	public void setNroDocumento(Integer nroDocumento) {
		this.nroDocumento = nroDocumento;
	}


	public String getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public String getNacionalidad() {
		return nacionalidad;
	}


	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Integer getTelefono() {
		return telefono;
	}


	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	
	
	
}
