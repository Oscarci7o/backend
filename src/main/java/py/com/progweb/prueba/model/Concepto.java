package py.com.progweb.prueba.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "concepto")

public class Concepto {
	@Id
	@Column(name="id_concepto")
	@Basic(optional = false)
	@GeneratedValue(generator = "conceptoSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "conceptoSec", sequenceName = "concepto_sec", allocationSize = 0)
	private Integer idConcepto;
	
	@Column(name = "descripcion")
	@Basic(optional = false)
	private String descripcion;
	
	@Column(name = "puntos_requeridos")
	@Basic(optional = false)
	private Integer puntosRequeridos;
	
	

	public Concepto() {

	}



	public Integer getIdConcepto() {
		return idConcepto;
	}



	public void setIdConcepto(Integer idConcepto) {
		this.idConcepto = idConcepto;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public Integer getPuntosRequeridos() {
		return puntosRequeridos;
	}



	public void setPuntosRequeridos(Integer puntosRequeridos) {
		this.puntosRequeridos = puntosRequeridos;
	}
	
	
	
}
