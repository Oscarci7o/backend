package py.com.progweb.prueba.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "bolsa")

public class Bolsa {
	@Id
	@Column(name="id_bolsa")
	@Basic(optional = false)
	@GeneratedValue(generator = "bolsaSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "bolsaSec", sequenceName = "bolsa_sec", allocationSize = 0)
	private Integer idBolsa;
	
	@Column(name = "id_cliente")
	@Basic(optional = false)
	private Integer idCliente;
	
	@Column(name = "fecha_asignacion")
	@Basic(optional = false)
	@Temporal(TemporalType.DATE)
	private Date fechaAsignacion;
	
	@Column(name = "fecha_caducidad")
	@Basic(optional = false)
	@Temporal(TemporalType.DATE)
	private Date fechaCaducidad;
	
	
	@Column(name = "puntaje_asignado")
	@Basic(optional = false)
	private Integer puntajeAsignado;
	
	@Column(name = "puntaje_utilizado")
	@Basic(optional = false)
	private Integer puntajeUtilizado;
	
	@Column(name = "saldo_puntos")
	@Basic(optional = false)
	private Integer saldoPuntos;
	
	@Column(name = "monto_operacion")
	@Basic(optional = false)
	private Integer montoOperacion;
	

	public Bolsa() {

	}


	public Integer getIdBolsa() {
		return idBolsa;
	}


	public void setIdBolsa(Integer idBolsa) {
		this.idBolsa = idBolsa;
	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}


	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}


	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}


	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}


	public Integer getPuntajeAsignado() {
		return puntajeAsignado;
	}


	public void setPuntajeAsignado(Integer puntajeAsignado) {
		this.puntajeAsignado = puntajeAsignado;
	}


	public Integer getPuntajeUtilizado() {
		return puntajeUtilizado;
	}


	public void setPuntajeUtilizado(Integer puntajeUtilizado) {
		this.puntajeUtilizado = puntajeUtilizado;
	}


	public Integer getSaldoPuntos() {
		return saldoPuntos;
	}


	public void setSaldoPuntos(Integer saldoPuntos) {
		this.saldoPuntos = saldoPuntos;
	}


	public Integer getMontoOperacion() {
		return montoOperacion;
	}


	public void setMontoOperacion(Integer montoOperacion) {
		this.montoOperacion = montoOperacion;
	}

	
	
	
}
