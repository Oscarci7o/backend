package py.com.progweb.prueba.model;



import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;

import javax.persistence.Table;


@Entity
@Table(name = "regla_puntos")

public class ReglaPuntos {
	@Id
	@Column(name = "puntos")
	@Basic(optional = false)
	private Integer puntos;
	
	@Column(name = "guaranies")
	@Basic(optional = false)
	private Integer guaranies;


	
	public ReglaPuntos() {
		
	}



	public Integer getPuntos() {
		return puntos;
	}



	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}



	public Integer getGuaranies() {
		return guaranies;
	}



	public void setGuaranies(Integer guaranies) {
		this.guaranies = guaranies;
	}


	
	
	
}
