package py.com.progweb.prueba.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "cabecera")

public class Cabecera {
	@Id
	@Column(name="id_cabecera")
	@Basic(optional = false)
	@GeneratedValue(generator = "cabeceraSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "cabeceraSec", sequenceName = "cabecera_sec", allocationSize = 0)
	private Integer idCabecera;
	
	@Column(name = "id_cliente")
	@Basic(optional = false)
	private Integer idCliente;
	
	@Column(name = "puntaje_usado")
	@Basic(optional = false)
	private Integer puntajeUsado;
	
	@Column(name = "fecha")
	@Basic(optional = false)
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	@Column(name = "concepto_uso")
	@Basic(optional = false)
	private Integer conceptoUso;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cabecera")
	private List<Detalle> listaDetalle;
	
	

	public Cabecera() {

	}



	public Integer getIdCabecera() {
		return idCabecera;
	}



	public void setIdCabecera(Integer idCabecera) {
		this.idCabecera = idCabecera;
	}



	public Integer getIdCliente() {
		return idCliente;
	}



	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}



	public Integer getPuntajeUsado() {
		return puntajeUsado;
	}



	public void setPuntajeUsado(Integer puntajeUsado) {
		this.puntajeUsado = puntajeUsado;
	}



	public Date getFecha() {
		return fecha;
	}



	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public Integer getConceptoUso() {
		return conceptoUso;
	}



	public void setConceptoUso(Integer conceptoUso) {
		this.conceptoUso = conceptoUso;
	}


	@JsonManagedReference
	public List<Detalle> getListaDetalle() {
		return listaDetalle;
	}



	public void setListaDetalle(List<Detalle> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}
	
	
	
	
}
