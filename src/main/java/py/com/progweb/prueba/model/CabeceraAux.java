package py.com.progweb.prueba.model;


public class CabeceraAux {
	
	private Integer idCliente;
	
	private Integer idConcepto;
	

	public CabeceraAux() {

	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Integer getIdConcepto() {
		return idConcepto;
	}


	public void setIdConcepto(Integer idConcepto) {
		this.idConcepto = idConcepto;
	}

	
	
	
}
