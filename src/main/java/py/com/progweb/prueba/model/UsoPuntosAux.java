package py.com.progweb.prueba.model;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class UsoPuntosAux {
	
	private Integer idConcepto;
	
	private Integer idCliente;
	
	private String fechaUso;
	

	public UsoPuntosAux() {

	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Integer getIdConcepto() {
		return idConcepto;
	}


	public void setIdConcepto(Integer idConcepto) {
		this.idConcepto = idConcepto;
	}


	public String getFechaUso() {
		return fechaUso;
	}


	public void setFechaUso(String fechaUso) {
		this.fechaUso = fechaUso;
	}
	
	

	
	
	
}
