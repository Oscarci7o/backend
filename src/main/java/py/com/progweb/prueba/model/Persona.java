package py.com.progweb.prueba.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.Hibernate;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "persona")

public class Persona {
	@Id
	@Column(name="id_persona")
	@Basic(optional = false)
	@GeneratedValue(generator = "personaSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "personaSec", sequenceName = "persona_sec", allocationSize = 0)
	private Integer idPersona;

	@Column(name = "nombre", length = 50)
	@Basic(optional = false)
	private String nombre;

	@Column(name = "apellido", length = 50)
	@Basic(optional = false)
	private String apellido;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "persona")
	private List<Agenda> listaAgenda;

	public Persona() {
		
	}

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@JsonManagedReference
	public List<Agenda> getListaAgenda() {
		return listaAgenda;
	}

	public void setListaAgenda(List<Agenda> listaAgenda) {
		this.listaAgenda = listaAgenda;
	}
	
	
	
}
