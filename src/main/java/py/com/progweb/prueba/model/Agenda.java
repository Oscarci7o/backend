package py.com.progweb.prueba.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "agenda")

public class Agenda {
	@Id
	@Column(name="id_agenda")
	@Basic(optional = false)
	@GeneratedValue(generator = "agendaSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "agendaSec", sequenceName = "agenda_sec", allocationSize = 0)
	private Integer idAgenda;

	@Column(name = "actividad", length = 200)
	@Basic(optional = false)
	private String actividad;

	@Column(name = "fecha")
	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
	@ManyToOne(optional = false)
	private Persona persona;
	
	public Agenda() {
		
	}

	public Integer getIdAgenda() {
		return idAgenda;
	}

	public void setIdAgenda(Integer idAgenda) {
		this.idAgenda = idAgenda;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@JsonBackReference
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	
	
	
}
